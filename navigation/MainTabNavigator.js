import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import DetailsScreen from '../screens/DetailsScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import { BorderlessButton } from 'react-native-gesture-handler';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Pays',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? 'ios-globe' : 'md-globe'
      }
    />
  ),
  tabBarOptions: {
    activeTintColor: '#f4511e',
    labelStyle: {
      fontSize: 12,
      fontWeight: 'bold',
    },
    style: {
      backgroundColor: '#000',
      paddingVertical: 4,
    },
  }
};

HomeStack.path = '';

const LinksStack = createStackNavigator(
  {
    Links: LinksScreen,
    Details: DetailsScreen,
  },
  config
);

LinksStack.navigationOptions = {
  tabBarLabel: 'Langue',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-happy' : 'md-happy'} />
  ),
  tabBarOptions: {
    activeTintColor: '#f4511e',
    labelStyle: {
      fontSize: 12,
      fontWeight: 'bold',
    },
    style: {
      backgroundColor: '#000',
      paddingVertical: 4,
    },
  }
};

LinksStack.path = '';

// const SettingsStack = createStackNavigator(
//   {
//     Settings: SettingsScreen,
//   },
//   config
// );

// SettingsStack.navigationOptions = {
//   tabBarLabel: 'Monaie',
//   tabBarIcon: ({ focused }) => (
//     <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-cash' : 'md-cash'} />
//   ),
//   tabBarOptions: {
//     activeTintColor: '#f4511e',
//     labelStyle: {
//       fontSize: 12,
//       fontWeight: 'bold',
//     },
//     style: {
//       backgroundColor: '#000',
//       paddingVertical: 4,
//     },
//   }
// };

// SettingsStack.path = '';

const tabNavigator = createBottomTabNavigator({
  HomeStack,
  LinksStack,
});

tabNavigator.path = '';

export default tabNavigator;
