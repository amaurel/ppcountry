import React, { Component } from "react";
import {
    StyleSheet,
    View,
    ScrollView,
    FlatList,
    Image,
    Text,
    TouchableOpacity,
    YellowBox
} from "react-native";

export default class Source extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: "Details",
            headerStyle: { backgroundColor: "#f4511e" },
            headerTitleStyle: { textAlign: "center", flex: 1 },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        };
    };

    FlatListItemSeparator = () => {
        return (
            <Text>, </Text>
        );
    }

    // Rendering of the app
    render() {
        console.disableYellowBox = true;
        // The page
        return (
            <ScrollView style={styles.container}>
                <Text style={styles.title}>
                    {this.props.navigation.state.params.country
                        ? this.props.navigation.state.params.country
                        : 'Country not found'}
                </Text>

                {/* <Image
                    style={{ width: 200, height: 200 }}
                    source={{ uri: this.props.navigation.state.params.flag }} />
                 
                 */}


                {/* Geography */}
                <View style={styles.separator} />
                <Text style={styles.subtitle}>
                    Geography
                </Text>
                <Text style={styles.information}>
                    Region : {this.props.navigation.state.params.region
                        ? this.props.navigation.state.params.region
                        : 'Not found'}
                </Text>
                <Text style={styles.information}>
                    Subregion : {this.props.navigation.state.params.subregion
                        ? this.props.navigation.state.params.subregion
                        : 'Not found'}
                </Text>
                <Text style={styles.information}>
                    Capital : {this.props.navigation.state.params.capital
                        ? this.props.navigation.state.params.capital
                        : 'Not found'}
                </Text>
                <Text style={styles.information}>
                    Area : {this.props.navigation.state.params.area + " km²"
                        ? this.props.navigation.state.params.area + " km²"
                        : 'Not found'}
                </Text>
                <FlatList
                    ListHeaderComponent={<Text style={styles.information}>Borders: </Text>}
                    data={this.props.navigation.state.params.borders}
                    horizontal={true}
                    ItemSeparatorComponent={this.FlatListItemSeparator}
                    renderItem={({ item }) => <Text style={styles.horizontalItem}>{item}</Text>}
                />

                {/* Population */}
                <View style={styles.separator} />
                <Text style={styles.subtitle}>
                    Population
                </Text>

                <Text style={styles.information}>
                    Population : {this.props.navigation.state.params.population + " inhabitants"
                        ? this.props.navigation.state.params.population + " inhabitants"
                        : 'Not found'}
                </Text>
                <Text style={styles.information}>
                    Demonym : {this.props.navigation.state.params.demonym
                        ? this.props.navigation.state.params.demonym
                        : 'Not found'}
                </Text>

                {/* Languages */}
                <View style={styles.separator} />
                <Text style={styles.subtitle}>
                    Languages
                </Text>
                <Text style={styles.information}>
                    Native language : {this.props.navigation.state.params.nativeLanguage
                        ? this.props.navigation.state.params.nativeLanguage
                        : 'Not found'}
                </Text>

                <FlatList
                    ListHeaderComponent={<Text style={styles.information}>Other languages : </Text>}
                    data={this.props.navigation.state.params.languages}
                    renderItem={({ item }) => <Text style={styles.verticalItem}>{item.name}</Text>}
                />

                {/* Currencies */}
                <View style={styles.separator} />
                <FlatList
                    ListHeaderComponent={
                        <Text style={styles.subtitle}>
                            Currencies
                        </Text>}
                    data={this.props.navigation.state.params.currencies}
                    renderItem={({ item }) => <Text style={styles.information}>{item.name} ({item.symbol})</Text>}
                />

                {/* Codes */}
                <View style={styles.separator} />
                <Text style={styles.subtitle}>
                    Codes
                </Text>
                <Text style={styles.information}>
                    Alpha2Code : {this.props.navigation.state.params.alpha2
                        ? this.props.navigation.state.params.alpha2
                        : 'Not found'}
                </Text>
                <Text style={styles.information}>
                    Alpha3Code : {this.props.navigation.state.params.alpha3
                        ? this.props.navigation.state.params.alpha3
                        : 'Not found'}
                </Text>
                <Text style={styles.information}>
                    Numeric code : {this.props.navigation.state.params.numeric
                        ? this.props.navigation.state.params.numeric
                        : 'Not found'}
                </Text>
                <Text style={styles.information}>
                    International code : {this.props.navigation.state.params.international
                        ? this.props.navigation.state.params.international
                        : 'Not found'}
                </Text>
                <Text style={styles.information}>
                    Top level domain : {this.props.navigation.state.params.domain
                        ? this.props.navigation.state.params.domain
                        : 'Not found'}
                </Text>
            </ScrollView >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    title: {
        fontWeight: "bold",
        fontSize: 30, 
        textAlign: "center",
        padding: 10,
    },
    subtitle: {
        fontWeight: "bold",
        fontSize: 22,
        textAlign: "left",
        padding: 10,
        backgroundColor: '#000',
        color: 'white',
        marginBottom: 10,
    },
    separator: {
        height: .5,
        width: "100%",
        backgroundColor: "rgba(0,0,0,0.5)",
        marginTop: 15,
    },
    information: {
        marginLeft: 10,
        fontSize: 16,
    },
    verticalItem: {
        marginLeft: 20,
        fontSize: 16,
    },
    horizontalItem: {
        marginLeft: 0,
        fontSize: 16,
    }
});
