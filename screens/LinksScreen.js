import React, { Component } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  FlatList,
  Button,
  TouchableOpacity,
  TouchableHighlight,
  View,
  SafeAreaView,
  ActivityIndicator,
  TextInput,
  YellowBox
} from "react-native";
import { ExpoLinksView } from "@expo/samples";

import { inject, observer } from "mobx-react";

class Langue extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Recherche par langue",
      headerStyle: {
        backgroundColor: "#f4511e"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "bold"
      }
    };
  };

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          marginVertical: 10,
          backgroundColor: "#000"
        }}
      />
    );
  };

  renderItemData = data => {
    return (
      <TouchableOpacity
        style={styles.list}
        onPress={() => {
          this.props.navigation.navigate("Details", {
            country: data.item.name,
            flag: data.item.flag,

            region: data.item.region,
            subregion: data.item.subregion,
            capital: data.item.capital,
            area: data.item.area,
            borders: data.item.borders,

            population: data.item.population,
            demonym: data.item.demonym,

            nativeLanguage: data.item.languages[0].name,
            languages: data.item.languages,

            currencies: data.item.currencies,

            alpha2: data.item.alpha2Code,
            alpha3: data.item.alpha3Code,
            numeric: data.item.numericCode,
            international: data.item.callingCodes[0],
            domain: data.item.topLevelDomain[0]
          })
        }}
      >
        <Text style={styles.title}>{data.item.name}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    console.disableYellowBox = true;
    const {
      dataSourceSpanish,
      dataSourceEnglish,
      dataSourceFrench,
      dataSourceChinese,
      dataSourceGerman,
      loadLang,
      loading,
      searching,
      lang,
      updateLang,
      setLang,
      dataFound,
      dataSource
    } = this.props.countryStore;

    if (!loading && (!searching || lang === "")) {
      return (
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <TextInput
            style={styles.textBox}
            placeholder="Ex: English"
            onChangeText={setLang}
          />
          <TouchableOpacity style={styles.btn} onPress={updateLang}>
            <Text style={styles.btnText}>Rechercher</Text>
          </TouchableOpacity>
          <TouchableHighlight style={styles.btn2} onPress={loadLang}>
            <Text style={styles.btnText2}>Afficher toutes les langues</Text>
          </TouchableHighlight>
          <SafeAreaView style={styles.container}>
            <Text style={styles.title2}>English 🇬🇧</Text>
            <FlatList
              data={dataSourceEnglish}
              style={{ flex: 2 }}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={country => this.renderItemData(country)}
              keyExtractor={country => country.name}
            />
            <Text style={styles.title3}>Spanish 🇪🇸</Text>
            <FlatList
              data={dataSourceSpanish}
              style={{ flex: 2 }}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={country => this.renderItemData(country)}
              keyExtractor={country => country.name}
            />
            <Text style={styles.title3}>Chinese 🇨🇳</Text>
            <FlatList
              data={dataSourceChinese}
              initialNumToRender={3}
              style={{ flex: 2 }}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={country => this.renderItemData(country)}
              keyExtractor={country => country.name}
            />
            <Text style={styles.title3}>French 🇫🇷</Text>
            <FlatList
              data={dataSourceFrench}
              initialNumToRender={3}
              style={{ flex: 2 }}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={country => this.renderItemData(country)}
              keyExtractor={country => country.name}
            />
            <Text style={styles.title3}>German 🇩🇪</Text>
            <FlatList
              data={dataSourceGerman}
              initialNumToRender={3}
              style={{ flex: 2 }}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={country => this.renderItemData(country)}
              keyExtractor={country => country.name}
            />
          </SafeAreaView>
        </ScrollView>
      );
    } else if (!loading) {
      return (
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <TextInput
            style={styles.textBox}
            placeholder="Ex: English"
            onChangeText={setLang}
          />
          <TouchableOpacity style={styles.btn} onPress={updateLang}>
            <Text style={styles.btnText}>Rechercher</Text>
          </TouchableOpacity>
          <SafeAreaView style={styles.container}>
            <Text style={styles.title2}>Langue recherchée: {lang}</Text>
            <FlatList
              data={dataFound}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={country => this.renderItemData(country)}
              keyExtractor={country => country.name.toString()}
            />
          </SafeAreaView>
        </ScrollView>
      );
    } else {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#f40" />
        </View>
      );
    }
  }
}

export default inject("countryStore")(observer(Langue));

const styles = StyleSheet.create({
  container: {
    paddingTop: 15,
    backgroundColor: "#fff"
  },
  title: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
  },
  title2: {
    fontSize: 20,
    fontWeight: "600",
    textAlign: "center",
    marginBottom: 20,
    padding: 10,
    backgroundColor: '#000',
    color: 'white'
  },
  title3: {
    fontSize: 20,
    fontWeight: "600",
    textAlign: "center",
    marginBottom: 10,
    marginTop: 40,
    padding: 10,
    backgroundColor: '#000',
    color: 'white'
  },
  containerFlex: {
    flex: 1,
    padding: 50
  },
  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  textBox: {
    borderBottomWidth: 1,
    borderColor: "#ccc",
    paddingTop: 10,
    fontSize: 22,
    margin: 10
  },
  btn: {
    backgroundColor: "#f4511e",
    margin: 10,
    padding: 10
  },
  btn2: {
    backgroundColor: "#000",
    margin: 10,
    padding: 10
  },
  btnText: {
    textAlign: "center",
    color: "white",
    fontSize: 20,
    padding: -5,
    fontWeight: "bold"
  },
  btnText2: {
    textAlign: "center",
    color: "white",
    fontSize: 12,
    padding: -10,
    fontWeight: "bold"
  }
});
