import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  ActivityIndicator,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  YellowBox,
} from "react-native";

import { inject, observer } from 'mobx-react';

class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Recherche par pays",
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    };
  };
  // Item separator
  FlatListItemSeparator = () => {
    return (
      <View style={{
        height: .5,
        width: "100%",
        backgroundColor: "rgba(0,0,0,0.5)",
      }}
      />
    );
  }

  // Rendering of an item
  renderItem = (data) =>
    <TouchableOpacity style={styles.list}
      onPress={() => {
        this.props.navigation.navigate("Details",
          {
            country: data.item.name,
            flag: data.item.flag,

            region: data.item.region,
            subregion: data.item.subregion,
            capital: data.item.capital,
            area: data.item.area,
            borders: data.item.borders,

            population: data.item.population,
            demonym: data.item.demonym,

            nativeLanguage: data.item.languages[0].name,
            languages: data.item.languages,

            currencies: data.item.currencies,
            
            alpha2: data.item.alpha2Code,
            alpha3: data.item.alpha3Code,
            numeric: data.item.numericCode,
            international:  data.item.callingCodes[0],
            domain:  data.item.topLevelDomain[0],
          })
      }}>
      <Text style={styles.title}>{data.item.name}</Text>
    </TouchableOpacity>


  // Rendering of the app
  render() {
    console.disableYellowBox = true;
    // Loading from store
    const { updatePays, loadCountry, countryData, loading, pays } = this.props.countryStore
    // console.log(countryData);
    // Loading component
    if (loading) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#f40" />
        </View>
      )
    }

    // The page
    return (
      <View style={styles.container}>
        {/* Search Bar */}
        <TextInput
          style={styles.textBox}
          placeholder="Entrez un pays"
          value={pays}
          onChangeText={updatePays}
        />
        <TouchableOpacity style={styles.btn} onPress={loadCountry}>
          <Text style={styles.btnText}>Rechercher</Text>
        </TouchableOpacity>
        <TouchableHighlight style={styles.btn2} onPress={loadCountry}>
          <Text style={styles.btnText2}>Afficher tous les pays</Text>
        </TouchableHighlight>
        {/* List */}
        <FlatList
          data={countryData}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          renderItem={item => this.renderItem(item)}
          keyExtractor={(item) => item.name.toString()}
        />
      </View>
    )
  }
}

export default inject("countryStore")(observer(HomeScreen));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  image: {
    width: 70,
    height: 70,
    textAlign: "center",

  },
  title: {
    fontWeight: "bold",
    fontSize: 30,
    textAlign: "center",
  },
  textBox: {
    borderBottomWidth: 1,
    borderColor: '#ccc',
    paddingTop: 10,
    fontSize: 22,
    margin: 10,
  },
  btn: {
    backgroundColor: '#f4511e',
    margin: 10,
    padding: 10,
  },
  btnText: {
    textAlign: "center",
    color: 'white',
    fontSize: 20,
    padding: -5,
    fontWeight: 'bold',
  },
  btnText2: {
    textAlign: "center",
    color: 'white',
    fontSize: 12,
    padding: -10,
    fontWeight: 'bold',
  },
  btn2: {
    backgroundColor: "#000",
    margin: 10,
    padding: 10
  },
  list: {
    paddingVertical: 4,
    margin: 5,
    backgroundColor: "#fff"
  }
});