# React_Projet_Pays

Ceci est un projet réalisé avec JavaScript et React Native. Il à été réalisé
par 3 étudiants en licence professionnelle DAWIN.

Cette application utilise l'API REST Countries (https://restcountries.eu/) 
pour fonctionner et permettre d'afficher des informations sur les pays.  


# Lien de la vidéo de présentation
https://youtu.be/3fJS5dHMa9w