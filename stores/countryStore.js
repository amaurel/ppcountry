import { action, observable, runInAction, decorate, computed } from "mobx";

// configure({ enforceActions: true });
let API_ALL_CONTENT = "https://restcountries.eu/rest/v2/all";
let API_LANG = "https://restcountries.eu/rest/v2/lang/";
let API_COUNTRY = "https://restcountries.eu/rest/v2/name/";
class CountryStore {
  countryData = [];
  loading = false;
  pays = "";
  dataSourceSpanish = [];
  dataSourceEnglish = [];
  dataSourceFrench = [];
  dataSourceChinese = [];
  dataSourceGerman = [];
  searching = false;
  lang = "";
  dataSource = [];
  dataFound = [];

  updatePays = pays => {
    this.pays = pays;
  };

  loadCountry = () => {
    if (this.pays === "") {
      fetch(API_ALL_CONTENT)
        .then(response => response.json())
        .then(data => this.setCountry(data), (this.loading = false));
    } else {
      fetch(API_COUNTRY + this.pays)
        .then(response => response.json())
        .then(data => this.setCountry(data), (this.loading = false));
    }
  };

  setCountry = data => {
    this.countryData = data;
  };

  loadLang = () => {
    fetch(API_ALL_CONTENT)
      .then(response => response.json())
      .then(responseJson => {
        this.dataSource = responseJson;
      })
      .catch(error => console.log(error));
    //Default language : english
    fetch(API_LANG + "eng")
      .then(response => response.json())
      .then(responseJson => {
        this.dataSourceEnglish = responseJson.slice(0, 3);
      })
      .catch(error => console.log(error));
    //Default language : spanish
    fetch(API_LANG + "es")
      .then(response => response.json())
      .then(responseJson => {
        this.dataSourceSpanish = responseJson.slice(0, 3);
      })
      .catch(error => console.log(error));
    //Default language : French
    fetch(API_LANG + "fr")
      .then(response => response.json())
      .then(responseJson => {
        this.dataSourceFrench = responseJson.slice(0, 3);
      })
      .catch(error => console.log(error));
    //Default language : Chinese
    fetch(API_LANG + "zh")
      .then(response => response.json())
      .then(responseJson => {
        this.dataSourceChinese = responseJson.slice(0, 3);
      })
      .catch(error => console.log(error));
    //Default language : German
    fetch(API_LANG + "de")
      .then(response => response.json())
      .then(responseJson => {
        this.dataSourceGerman = responseJson.slice(0, 3);
      })
      .catch(error => console.log(error));
  };

  findByLanguage = (data, search) => {
    let result = [];
    data.forEach(item => {
      item.languages.forEach(language => {
        if (language.name == search) {
          result.push(item);
        }
      });
    });
    if (result.length == 0) {
      let emptyItem = { name: "No result" };
      result.push(emptyItem);
    }
    return result;
  };

  updateLang = () => {
    this.dataFound = [];
    this.searching = true;
    // const { lang } = this.lang;
    this.dataFound = this.findByLanguage(this.dataSource, this.lang);
    console.log(JSON.stringify(this.dataFound));
    this.loadLang();
  };

  setLang = lang => {
    this.lang = lang;
  };

  setLangEn = responseJson => {
    this.dataSourceEnglish = responseJson;
  };
  setLangSp = responseJson => {
    this.dataSourceSpanish = responseJson;
  };
  setLangFr = responseJson => {
    this.dataSourceFrench = responseJson;
  };
  setLangCh = responseJson => {
    this.dataSourceChinese = responseJson;
  };
  setLangGe = responseJson => {
    this.dataSourceGerman = responseJson;
  };
}

decorate(CountryStore, {
  countryData: observable,
  loading: observable,
  pays: observable,
  updatePays: action,
  loadCountry: action,
  setCountry: action,
  dataSourceSpanish: observable,
  dataSourceEnglish: observable,
  dataSourceFrench: observable,
  dataSourceChinese: observable,
  dataSourceGerman: observable,
  lang: observable,
  loadLang: action,
  setLang: action,
  findByLanguage: action,
  setLangEn: action,
  setLangSp: action,
  setLangFr: action,
  setLangCh: action,
  setLangGe: action,
  searching: observable,
  dataSource: observable,
  dataFound: observable,
  updateLang: action
});

export default new CountryStore();
